import firebase from 'firebase';

class FirebaseService {

	loginWithUserPass ( email, password ) {
		return firebase
			.auth()
			.signInWithEmailAndPassword(email, password);
	}

	loginWithTwitter () {
		let provider = new firebase.auth.TwitterAuthProvider();
		return firebase
			.auth()
			.signInWithPopup(provider);
	}

	loginWithFacebook () {
		let provider = new firebase.auth.FacebookAuthProvider();
		return firebase
			.auth()
			.signInWithPopup(provider);
	}

	loginWithGithub () {
			let provider = new firebase.auth.GithubAuthProvider();
			return firebase
				.auth()
				.signInWithPopup(provider);
	}

	loginWithEmailPass ( email, password ) {
		return firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
	}

	signup ( email, password ) {
		return firebase
			.auth()
	    .createUserWithEmailAndPassword(email, password);
	}
}

let firebaseService = new FirebaseService();
export default firebaseService;