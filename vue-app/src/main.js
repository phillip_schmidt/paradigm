require(`./themes/app.${__THEME}.styl`);

/* IE 11 / Edge Support */
require(`quasar/dist/quasar.ie`);
require(`quasar/dist/quasar.ie.${__THEME}.css`);

import Vue from 'vue'
import Quasar, * as All from 'quasar'
import Vuelidate from 'vuelidate'
import firebase from 'firebase'
import router from './router'
import settings from './settings/app.settings';

firebase.initializeApp(settings.firebase);

Vue.config.productionTip = false;

Vue.use(Quasar, {
	components: All,
	directives: All
});

Vue.use(Vuelidate);

Vue.mixin({
	methods: {
		go ( path, options ) {
	  	this.$router.push({ path, options });
	  }
	}
});

if (__THEME === 'mat') {
  require('quasar-extras/roboto-font');
}

import 'quasar-extras/material-icons'
import 'quasar-extras/fontawesome'
import 'quasar-extras/animate'

Quasar.start(() => {
  new Vue({
    el: '#q-app',
    router,
    render: h => h(require('./app'))
  });
});
