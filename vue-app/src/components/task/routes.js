import { load } from 'src/helpers'
export default [
  {
    path: '/task', 
    component: load('common/abstract'),
    children: [
      {
        path: 'list',
        component: load('task/task-list')
      },
      {
        path: 'create',
        component: load('task/task-create')
      },
      {
        path: ':id',
        component: load('task/task-edit')
      }
    ]
  }
]