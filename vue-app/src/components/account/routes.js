import { load } from 'src/helpers';

export default [
	{
    path: '/login',
    component: load('account/login')
  },
  {
    path: '/register',
    component: load('account/register')
  }
];