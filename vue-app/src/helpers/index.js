let helpers = {
	load ( component ) {
	  return () => System.import(`components/${component}.vue`)
	}
};

export const load = helpers.load;
export default helpers.load; 